"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const discord_js_1 = require("discord.js");
let answers = require("../../../API/JSON/8Ball.json");
const { eightballIcon } = require('../../../API/GLOBALS/globalvars.js');
class EightBallCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "8ball",
            aliases: ["eightball"],
            group: "fun",
            memberName: "8ball",
            description: "SEE A BOT'S INFORMATION EZ",
            args: [
                {
                    key: "question",
                    type: "string",
                    prompt: "WHAT DO YOU WANNA ASK MEEEEEEEEE",
                }
            ]
        });
    }
    run(msg, args) {
        return __awaiter(this, void 0, void 0, function* () {
            const embed = new discord_js_1.MessageEmbed()
                .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: 'webp' }))
                .setThumbnail(eightballIcon)
                .setTitle(args.question)
                .addField('ANSWER', `**${answers[Math.floor(Math.random() * answers.length)]}**`);
            return msg.embed(embed);
        });
    }
}
exports.default = EightBallCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiOGJhbGwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvY29tbWFuZHMvR2FtZXMvOGJhbGwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSw2REFBK0U7QUFDL0UsMkNBQW1EO0FBQ25ELElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0FBQ3RELE1BQU0sRUFBRSxhQUFhLEVBQUUsR0FBRyxPQUFPLENBQUMsb0NBQW9DLENBQUMsQ0FBQztBQU14RSxNQUFxQixnQkFBaUIsU0FBUSw2QkFBTztJQUNuRCxZQUFZLE1BQXNCO1FBQ2hDLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDWixJQUFJLEVBQUUsT0FBTztZQUNiLE9BQU8sRUFBRSxDQUFDLFdBQVcsQ0FBQztZQUN0QixLQUFLLEVBQUUsS0FBSztZQUNaLFVBQVUsRUFBRSxPQUFPO1lBQ25CLFdBQVcsRUFBRSw0QkFBNEI7WUFDekMsSUFBSSxFQUFFO2dCQUNKO29CQUNFLEdBQUcsRUFBRSxVQUFVO29CQUNmLElBQUksRUFBRSxRQUFRO29CQUNkLE1BQU0sRUFBRSxrQ0FBa0M7aUJBQzNDO2FBQ0Y7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUssR0FBRyxDQUFDLEdBQW9CLEVBQUUsSUFBZ0I7O1lBQzlDLE1BQU0sS0FBSyxHQUFHLElBQUkseUJBQVksRUFBRTtpQkFDekIsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztpQkFDL0UsWUFBWSxDQUFDLGFBQWEsQ0FBQztpQkFDM0IsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7aUJBQ3ZCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsS0FBSyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hGLE9BQU8sR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixDQUFDO0tBQUE7Q0FDRjtBQTFCRCxtQ0EwQkMifQ==