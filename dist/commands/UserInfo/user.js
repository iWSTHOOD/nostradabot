"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const discord_js_1 = require("discord.js");
class UserInfoCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "user",
            aliases: ["me", "user_info", "him"],
            group: "userinfo",
            memberName: "user",
            description: "SEE A USER'S INFORMATION EZ",
            guildOnly: true,
            args: [
                {
                    key: "user",
                    type: "user",
                    prompt: "who?",
                    default: (msg) => msg.author
                }
            ]
        });
    }
    run(msg, args) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = args.user;
            const embed = new discord_js_1.MessageEmbed()
                .setAuthor(`${user.username}`, user.displayAvatarURL({ format: 'webp' }))
                .setTitle(`${user.username}'S INFROMATION`)
                .setThumbnail(user.displayAvatarURL({ format: 'png', size: 2048 }))
                .addField("ISB0T?", user.bot ? 'YES' : 'NAH', true)
                .addField("ACCOUNT CREATED AT", user.createdAt.toUTCString(), true)
                .addField("USER DISCRIMINATOR", user.discriminator, true)
                .addField("USER ID", user.id, true)
                .addField("USER PRESENCE", user.presence.status.toUpperCase(), true)
                .addField("USER TAG", user.tag.toUpperCase(), true);
            try {
                const member = yield msg.guild.members.fetch(user.id);
                if (!member) {
                    msg.reply("SORRY BRO BUT THIS MEMBER EITHER GOTTEN DELETED OR BANNED FROM THE GUILD IF YOU THINK THIS IS AN ERROR PLEASE CONTACT THE DEVELOPER " + "STEPHANIE#4687");
                }
                else {
                    const roles = member.roles
                        .filter((role) => role.name !== "@everyone")
                        .sort((a, b) => b.position - a.position)
                        .map((role) => role.name);
                    embed
                        .setColor(member.displayHexColor)
                        .addField("USER BANNABLE?", member.bannable ? 'YES' : 'NOPE', true)
                        .addField("USER NICKNAME", member.nickname || "NONE", true)
                        .addField("USER'S HIGHEST ROLE", member.roles.highest.name === '@everyone' ? 'THIS USER GOT NO ROLES' : member.roles.highest.name, true)
                        .addField("USER'S HOIST ROLE", member.roles.hoist ? member.roles.hoist.name : 'THIS USER GOT NO HOIST ROLES', true)
                        .addField(`USER'S ROLES ${roles.length}`, roles.join(', '));
                }
            }
            catch (error) {
                embed.setFooter("THIS IS WHAT I COULD FETCH FROM THE MEMBER :(");
            }
            return msg.embed(embed);
        });
    }
}
exports.default = UserInfoCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21tYW5kcy9Vc2VySW5mby91c2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsNkRBQStFO0FBQy9FLDJDQUErRDtBQU0vRCxNQUFxQixlQUFnQixTQUFRLDZCQUFPO0lBQ2hELFlBQVksTUFBc0I7UUFDOUIsS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNWLElBQUksRUFBRSxNQUFNO1lBQ1osT0FBTyxFQUFFLENBQUMsSUFBSSxFQUFFLFdBQVcsRUFBRSxLQUFLLENBQUM7WUFDbkMsS0FBSyxFQUFFLFVBQVU7WUFDakIsVUFBVSxFQUFFLE1BQU07WUFDbEIsV0FBVyxFQUFFLDZCQUE2QjtZQUMxQyxTQUFTLEVBQUUsSUFBSTtZQUNmLElBQUksRUFBRTtnQkFDRjtvQkFDSSxHQUFHLEVBQUUsTUFBTTtvQkFDWCxJQUFJLEVBQUUsTUFBTTtvQkFDWixNQUFNLEVBQUUsTUFBTTtvQkFDZCxPQUFPLEVBQUUsQ0FBQyxHQUFvQixFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsTUFBTTtpQkFDaEQ7YUFDSjtTQUNKLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFSyxHQUFHLENBQUMsR0FBb0IsRUFBRSxJQUFjOztZQUMxQyxNQUFNLElBQUksR0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzdCLE1BQU0sS0FBSyxHQUFHLElBQUkseUJBQVksRUFBRTtpQkFDekIsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO2lCQUN4RSxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxnQkFBZ0IsQ0FBQztpQkFDMUMsWUFBWSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQ3JFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2lCQUNsRCxRQUFRLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLENBQUM7aUJBQ2xFLFFBQVEsQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQztpQkFDeEQsUUFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQztpQkFDbEMsUUFBUSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLENBQUM7aUJBQ25FLFFBQVEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQTtZQUM3RCxJQUFJO2dCQUNBLE1BQU0sTUFBTSxHQUFHLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDdEQsSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDVCxHQUFHLENBQUMsS0FBSyxDQUFDLHNJQUFzSSxHQUFHLGdCQUFnQixDQUFDLENBQUM7aUJBQ3hLO3FCQUFNO29CQUNILE1BQU0sS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLO3lCQUNELE1BQU0sQ0FBQyxDQUFDLElBQVUsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxXQUFXLENBQUM7eUJBQ2pELElBQUksQ0FBQyxDQUFDLENBQU8sRUFBRSxDQUFPLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQzt5QkFDbkQsR0FBRyxDQUFDLENBQUMsSUFBVSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3hELEtBQUs7eUJBQ0osUUFBUSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUM7eUJBQ2hDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUM7eUJBQ2xFLFFBQVEsQ0FBQyxlQUFlLEVBQUUsTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLEVBQUUsSUFBSSxDQUFDO3lCQUMxRCxRQUFRLENBQUMscUJBQXFCLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUM7eUJBQ3ZJLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyw4QkFBOEIsRUFBRSxJQUFJLENBQUM7eUJBQ2xILFFBQVEsQ0FBQyxnQkFBZ0IsS0FBSyxDQUFDLE1BQU0sRUFBRSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDL0Q7YUFDSjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNaLEtBQUssQ0FBQyxTQUFTLENBQUMsK0NBQStDLENBQUMsQ0FBQzthQUNwRTtZQUNHLE9BQU8sR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1QixDQUFDO0tBQUE7Q0FDSjtBQXRERCxrQ0FzREMifQ==