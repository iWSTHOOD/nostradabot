"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const discord_js_1 = require("discord.js");
class BotInfoCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "bot",
            aliases: ["botinfo", "bot_info"],
            group: "userinfo",
            memberName: "bot",
            description: "SEE A BOT'S INFORMATION EZ"
        });
    }
    run(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            const NostradabotColor = yield msg.guild.members.fetch(this.client.user.id);
            const embed = new discord_js_1.MessageEmbed()
                .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: 'webp' }))
                .setTitle("INFORMATIONS ABOUT ME AND THE DEV TEAAAAAAM")
                .setColor(NostradabotColor ? NostradabotColor.roles.highest.color : 'ffc0cb')
                .setThumbnail(this.client.user.displayAvatarURL({ format: 'png', size: 2048 }))
                .setDescription("***SUP BITCHES ISSA ME NOSTRADABOT I AM A GENERAL PURPOSE BOT OK MADE BY WSTHOOD AND XRIDR***")
                .addField("BOT NAME", this.client.user.username, true)
                .addField("AM I A BOT?", "OFC U FUCKING DUMBASS", true)
                .addField("CREATED AT", this.client.user.createdAt.toUTCString(), true)
                .addField("I AM", this.client.user.presence.status.toString(), true);
            return msg.embed(embed);
        });
    }
}
exports.default = BotInfoCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm90LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL1VzZXJJbmZvL2JvdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLDZEQUErRTtBQUMvRSwyQ0FBK0Q7QUFFL0QsTUFBcUIsY0FBZSxTQUFRLDZCQUFPO0lBQ2pELFlBQVksTUFBc0I7UUFDaEMsS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNaLElBQUksRUFBRSxLQUFLO1lBQ1gsT0FBTyxFQUFFLENBQUMsU0FBUyxFQUFFLFVBQVUsQ0FBQztZQUNoQyxLQUFLLEVBQUUsVUFBVTtZQUNqQixVQUFVLEVBQUUsS0FBSztZQUNqQixXQUFXLEVBQUUsNEJBQTRCO1NBQzFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFSyxHQUFHLENBQUMsR0FBb0I7O1lBQzVCLE1BQU0sZ0JBQWdCLEdBQUcsTUFBTSxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDNUUsTUFBTSxLQUFLLEdBQUcsSUFBSSx5QkFBWSxFQUFFO2lCQUN6QixTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO2lCQUMvRSxRQUFRLENBQUMsNkNBQTZDLENBQUM7aUJBQ3ZELFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztpQkFDNUUsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztpQkFDOUUsY0FBYyxDQUFDLCtGQUErRixDQUFDO2lCQUMvRyxRQUFRLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUM7aUJBQ3JELFFBQVEsQ0FBQyxhQUFhLEVBQUUsdUJBQXVCLEVBQUUsSUFBSSxDQUFDO2lCQUN0RCxRQUFRLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLENBQUM7aUJBQ3RFLFFBQVEsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQTtZQUMxRSxPQUFPLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUIsQ0FBQztLQUFBO0NBQ0Y7QUF6QkQsaUNBeUJDIn0=