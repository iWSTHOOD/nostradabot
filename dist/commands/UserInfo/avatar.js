"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const discord_js_1 = require("discord.js");
class AvatarCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "avatar",
            aliases: ["pfp", "pdp", "user_avatar", "user_pfp"],
            group: "userinfo",
            memberName: "avatar",
            description: "SEE AND DOWNLOAD A USER'S AVATAR",
            throttling: {
                usages: 1,
                duration: 4
            },
            args: [
                {
                    key: "user",
                    prompt: "whom",
                    type: "user",
                    default: (msg) => msg.author
                },
                {
                    key: "size",
                    prompt: "nani",
                    type: "integer",
                    oneOf: [16, 32, 64, 128, 256, 512, 1024, 2048],
                    default: 2048
                },
                {
                    key: "format",
                    prompt: "what format",
                    type: "string",
                    oneOf: ["webp", "png", "jpg", "gif"],
                    default: 'png'
                }
            ]
        });
    }
    run(msg, args) {
        return __awaiter(this, void 0, void 0, function* () {
            const image = new discord_js_1.MessageAttachment(args.user.displayAvatarURL({ size: args.size, format: args.format }));
            yield msg.channel.send(image);
            let embed = new discord_js_1.MessageEmbed()
                .setDescription(`IF YOU WANT TO DOWNLOAD THE AVATAR AS [${args.format.toString().toUpperCase()}](${args.user.displayAvatarURL({ format: args.format, size: args.size }).toString()})`);
            return msg.embed(embed);
        });
    }
}
exports.default = AvatarCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXZhdGFyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL1VzZXJJbmZvL2F2YXRhci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLDZEQUErRTtBQUMvRSwyQ0FBaUc7QUFRakcsTUFBcUIsYUFBYyxTQUFRLDZCQUFPO0lBQzlDLFlBQVksTUFBc0I7UUFDOUIsS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNWLElBQUksRUFBRSxRQUFRO1lBQ2QsT0FBTyxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxhQUFhLEVBQUUsVUFBVSxDQUFDO1lBQ2xELEtBQUssRUFBRSxVQUFVO1lBQ2pCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLFdBQVcsRUFBRSxrQ0FBa0M7WUFDL0MsVUFBVSxFQUFFO2dCQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNULFFBQVEsRUFBRSxDQUFDO2FBQ2Q7WUFDRCxJQUFJLEVBQUU7Z0JBQ0Y7b0JBQ0ksR0FBRyxFQUFFLE1BQU07b0JBQ1gsTUFBTSxFQUFFLE1BQU07b0JBQ2QsSUFBSSxFQUFFLE1BQU07b0JBQ1osT0FBTyxFQUFFLENBQUMsR0FBb0IsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU07aUJBQ2hEO2dCQUNEO29CQUNJLEdBQUcsRUFBRSxNQUFNO29CQUNYLE1BQU0sRUFBRSxNQUFNO29CQUNkLElBQUksRUFBRSxTQUFTO29CQUNmLEtBQUssRUFBRSxDQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUU7b0JBQ2hELE9BQU8sRUFBRSxJQUFJO2lCQUNoQjtnQkFDRDtvQkFDSSxHQUFHLEVBQUUsUUFBUTtvQkFDYixNQUFNLEVBQUUsYUFBYTtvQkFDckIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLENBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFFO29CQUN0QyxPQUFPLEVBQUUsS0FBSztpQkFDakI7YUFDSjtTQUNKLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFSyxHQUFHLENBQUMsR0FBb0IsRUFBRSxJQUF3Qjs7WUFDcEQsTUFBTSxLQUFLLEdBQXNCLElBQUksOEJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdILE1BQU0sR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDOUIsSUFBSSxLQUFLLEdBQUcsSUFBSSx5QkFBWSxFQUFFO2lCQUN6QixjQUFjLENBQUMsMENBQTBDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsV0FBVyxFQUFFLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDM0wsT0FBTyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzVCLENBQUM7S0FBQTtDQUNKO0FBNUNELGdDQTRDQyJ9