"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
class RandomFrogCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "frog",
            aliases: ["random_frog", "phrog", "difda3a"],
            group: "random",
            memberName: "difda3a",
            description: "GIVES U EZ CAT",
        });
    }
    run(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            function pad(n) {
                let padding = 4;
                let zeroes = new Array(padding + 1).join("0");
                return (zeroes + n).slice(-padding);
            }
            try {
                const number_random = Math.floor(Math.random() * 55);
                const message = msg.say("LOOKING FOR PHROGZ");
                yield msg.say({
                    files: [`http://www.allaboutfrogs.org/funstuff/random/${pad(number_random)}.jpg`]
                });
                yield message.then((response) => {
                    response.delete();
                });
            }
            catch (e) {
                return msg.say(e.message);
            }
        });
    }
}
exports.default = RandomFrogCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnJvZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21tYW5kcy9SYW5kb20vZnJvZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLDZEQUErRTtBQUcvRSxNQUFxQixpQkFBa0IsU0FBUSw2QkFBTztJQUNsRCxZQUFZLE1BQXNCO1FBQzlCLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDVixJQUFJLEVBQUUsTUFBTTtZQUNaLE9BQU8sRUFBRSxDQUFDLGFBQWEsRUFBRSxPQUFPLEVBQUUsU0FBUyxDQUFDO1lBQzVDLEtBQUssRUFBRSxRQUFRO1lBQ2YsVUFBVSxFQUFFLFNBQVM7WUFDckIsV0FBVyxFQUFFLGdCQUFnQjtTQUNoQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sR0FBRyxDQUFDLEdBQW9COztZQUMxQixTQUFTLEdBQUcsQ0FBQyxDQUFTO2dCQUNsQixJQUFJLE9BQU8sR0FBVyxDQUFDLENBQUM7Z0JBQ3hCLElBQUksTUFBTSxHQUFXLElBQUksS0FBSyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3RELE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEMsQ0FBQztZQUVGLElBQUk7Z0JBQ0EsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQ3JELE1BQU0sT0FBTyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDO29CQUNWLEtBQUssRUFBRSxDQUFDLGdEQUFnRCxHQUFHLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztpQkFDcEYsQ0FBQyxDQUFDO2dCQUNILE1BQU0sT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQTZCLEVBQUUsRUFBRTtvQkFFakQsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUN0QixDQUFDLENBQUMsQ0FBQzthQUNOO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1IsT0FBTyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUM3QjtRQUNMLENBQUM7S0FBQTtDQUNKO0FBaENELG9DQWdDQyJ9