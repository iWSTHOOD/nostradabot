"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const discord_js_1 = require("discord.js");
let answers = require("../../../API/JSON/8Ball.json");
const globalvars_1 = require("../../../API/GLOBALS/globalvars");
class EightBallCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "8ball",
            aliases: ["eightball"],
            group: "fun",
            memberName: "8ball",
            description: "SEE A BOT'S INFORMATION EZ",
            args: [
                {
                    key: "question",
                    type: "string",
                    prompt: "WHAT DO YOU WANNA ASK MEEEEEEEEE",
                }
            ]
        });
    }
    async run(msg, args) {
        const embed = new discord_js_1.MessageEmbed()
            .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: 'webp' }))
            .setThumbnail(globalvars_1.eightballIcon)
            .setTitle(args.question)
            .addField('ANSWER', `**${answers[Math.ceil(Math.random() * answers.length)]}**`);
        return msg.embed(embed);
    }
}
exports.default = EightBallCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiOGJhbGwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvY29tbWFuZHMvR2FtZXMvOGJhbGwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw2REFBK0U7QUFDL0UsMkNBQW1EO0FBQ25ELElBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0FBQ3RELGdFQUFnRTtBQU1oRSxNQUFxQixnQkFBaUIsU0FBUSw2QkFBTztJQUNuRCxZQUFZLE1BQXNCO1FBQ2hDLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDWixJQUFJLEVBQUUsT0FBTztZQUNiLE9BQU8sRUFBRSxDQUFDLFdBQVcsQ0FBQztZQUN0QixLQUFLLEVBQUUsS0FBSztZQUNaLFVBQVUsRUFBRSxPQUFPO1lBQ25CLFdBQVcsRUFBRSw0QkFBNEI7WUFDekMsSUFBSSxFQUFFO2dCQUNKO29CQUNFLEdBQUcsRUFBRSxVQUFVO29CQUNmLElBQUksRUFBRSxRQUFRO29CQUNkLE1BQU0sRUFBRSxrQ0FBa0M7aUJBQzNDO2FBQ0Y7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFvQixFQUFFLElBQWdCO1FBQzlDLE1BQU0sS0FBSyxHQUFHLElBQUkseUJBQVksRUFBRTthQUN6QixTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO2FBQy9FLFlBQVksQ0FBQywwQkFBYSxDQUFDO2FBQzNCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQ3ZCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsS0FBSyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZGLE9BQU8sR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMxQixDQUFDO0NBQ0Y7QUExQkQsbUNBMEJDIn0=