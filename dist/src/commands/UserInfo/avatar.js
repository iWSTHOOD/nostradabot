"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const discord_js_1 = require("discord.js");
class AvatarCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "avatar",
            aliases: ["pfp", "pdp", "user_avatar", "user_pfp"],
            group: "userinfo",
            memberName: "avatar",
            description: "SEE AND DOWNLOAD A USER'S AVATAR",
            throttling: {
                usages: 1,
                duration: 4
            },
            args: [
                {
                    key: "user",
                    prompt: "whom",
                    type: "user",
                    default: (msg) => msg.author
                },
                {
                    key: "size",
                    prompt: "nani",
                    type: "integer",
                    oneOf: [16, 32, 64, 128, 256, 512, 1024, 2048],
                    default: 2048
                },
                {
                    key: "format",
                    prompt: "what format",
                    type: "string",
                    oneOf: ["webp", "png", "jpg", "gif"],
                    default: 'png'
                }
            ]
        });
    }
    async run(msg, args) {
        const image = new discord_js_1.MessageAttachment(args.user.displayAvatarURL({ size: args.size, format: args.format }));
        await msg.channel.send(image);
        let embed = new discord_js_1.MessageEmbed()
            .setDescription(`IF YOU WANT TO DOWNLOAD THE AVATAR AS [${args.format.toString().toUpperCase()}](${args.user.displayAvatarURL({ format: args.format, size: args.size }).toString()})`);
        return msg.embed(embed);
    }
}
exports.default = AvatarCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXZhdGFyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2NvbW1hbmRzL1VzZXJJbmZvL2F2YXRhci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDZEQUErRTtBQUMvRSwyQ0FBaUc7QUFRakcsTUFBcUIsYUFBYyxTQUFRLDZCQUFPO0lBQzlDLFlBQVksTUFBc0I7UUFDOUIsS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNWLElBQUksRUFBRSxRQUFRO1lBQ2QsT0FBTyxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxhQUFhLEVBQUUsVUFBVSxDQUFDO1lBQ2xELEtBQUssRUFBRSxVQUFVO1lBQ2pCLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLFdBQVcsRUFBRSxrQ0FBa0M7WUFDL0MsVUFBVSxFQUFFO2dCQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNULFFBQVEsRUFBRSxDQUFDO2FBQ2Q7WUFDRCxJQUFJLEVBQUU7Z0JBQ0Y7b0JBQ0ksR0FBRyxFQUFFLE1BQU07b0JBQ1gsTUFBTSxFQUFFLE1BQU07b0JBQ2QsSUFBSSxFQUFFLE1BQU07b0JBQ1osT0FBTyxFQUFFLENBQUMsR0FBb0IsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU07aUJBQ2hEO2dCQUNEO29CQUNJLEdBQUcsRUFBRSxNQUFNO29CQUNYLE1BQU0sRUFBRSxNQUFNO29CQUNkLElBQUksRUFBRSxTQUFTO29CQUNmLEtBQUssRUFBRSxDQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUU7b0JBQ2hELE9BQU8sRUFBRSxJQUFJO2lCQUNoQjtnQkFDRDtvQkFDSSxHQUFHLEVBQUUsUUFBUTtvQkFDYixNQUFNLEVBQUUsYUFBYTtvQkFDckIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLENBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFFO29CQUN0QyxPQUFPLEVBQUUsS0FBSztpQkFDakI7YUFDSjtTQUNKLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQW9CLEVBQUUsSUFBd0I7UUFDcEQsTUFBTSxLQUFLLEdBQXNCLElBQUksOEJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdILE1BQU0sR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsSUFBSSxLQUFLLEdBQUcsSUFBSSx5QkFBWSxFQUFFO2FBQ3pCLGNBQWMsQ0FBQywwQ0FBMEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMzTCxPQUFPLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDNUIsQ0FBQztDQUNKO0FBNUNELGdDQTRDQyJ9