import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message } from 'discord.js';
import axios, { AxiosResponse } from "axios";

export default class RandomDogsCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: "dog",
            aliases: ["random_dog", "anass"],
            group: "random",
            memberName: "dog",
            description: "GIVES U EZ CAT",
        });
    }

    async run(msg: CommandoMessage): Promise<Message | Message[]> {
        try {
            const response: AxiosResponse = await axios.get("https://dog.ceo/api/breeds/image/random");
            const message = msg.say("LOOKING FOR 3BYA");
            await msg.say({
                files: [response.data.message]
            });
            await message.then((response: Message | Message[]) => {
                //@ts-ignore
                response.delete();
            });
        } catch (e) {
            return msg.say(e.message);
        }
    }
}
