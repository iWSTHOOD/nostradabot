import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message } from 'discord.js';

export default class RandomFrogCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: "frog",
            aliases: ["random_frog", "phrog", "difda3a"],
            group: "random",
            memberName: "difda3a",
            description: "GIVES U EZ CAT",
        });
    }

     async run(msg: CommandoMessage): Promise<Message | Message[]> {
         function pad(n: number): string {
             let padding: number = 4;
             let zeroes: string = new Array(padding + 1).join("0");
             return (zeroes + n).slice(-padding);
         }

        try {
            const number_random = Math.floor(Math.random() * 55);
            const message = msg.say("LOOKING FOR PHROGZ");
            await msg.say({
                files: [`http://www.allaboutfrogs.org/funstuff/random/${pad(number_random)}.jpg`]
            });
            await message.then((response: Message | Message[]) => {
                //@ts-ignore
                response.delete();
            });
        } catch (e) {
            return msg.say(e.message);
        }
    }
}
