import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message, MessageEmbed, User } from "discord.js";
import axios, { AxiosResponse } from "axios";

interface IPatArg {
    user?: User;
}

export default class HugGifCommand extends Command {
    public constructor(client: CommandoClient) {
        super(client, {
            name: "hug",
            aliases: ["hug_gif", "hugme", "3n9"],
            group: "gif",
            memberName: "hug",
            description: "HUGS THE MENTIONED USER",
            details:
                "HUGS THE MENTIONED USER AND IF THE MENTIONED USER'S ID EQUALS THE MESSAGE AUTHOR'S ID THEN NOSTRADABOT REPLIES WITH ***AWWWWWWN SORRY TO RUIN IT FOR YOU BUD BUT YOU CAN'T HUG YOURSELF I CAN HUG YOU INSTEAD :3*** ELSE NOSTRADABOT REPLIES ***THE MESSAGE AUTHOR HUGGED THE MENTIONED USER***",
            clientPermissions: ["EMBED_LINKS", "SEND_MESSAGES"],
            args: [
                {
                    key: "user",
                    type: "user",
                    prompt: "WHO DO YOU WANNA HUG BRUV?"
                }
            ]
        });
    }

    async run(msg: CommandoMessage, args: IPatArg) {
        try {
            const response: AxiosResponse = await axios.get(
                "https://some-random-api.ml/animu/hug"
            );
            const user: User = args.user;
            let message: string;
            if (user.id === msg.author.id) {
                const embed = new MessageEmbed()
                    .setAuthor(
                        msg.author.username,
                        msg.author.displayAvatarURL({ format: "webp" })
                    )
                    .setTitle(
                        "AWWWWWWN SORRY TO RUIN IT FOR YOU BUD BUT YOU CAN'T HUG YOURSELF I CAN HUG YOU INSTEAD :3"
                    )
                    .setImage(response.data.link)
                    .setFooter(user.username, user.displayAvatarURL({ format: "webp" }));
                await msg.embed(embed);
            } else if (user.id !== msg.author.id && user.id !== this.client.user.id) {
                message = `${msg.author.username} JUST HUGGED ${user.username}`;
                const embed = new MessageEmbed()
                    .setAuthor(
                        msg.author.username,
                        msg.author.displayAvatarURL({ format: "webp" })
                    )
                    .setTitle(message)
                    .setImage(response.data.link)
                    .setFooter(user.username, user.displayAvatarURL({ format: "webp" }));
                await msg.embed(embed);
            } else if (user.id === this.client.user.id) {
                message = `AWWWWWWWWWWWWN THANK YOU WHGE9URHEIHGIREHGIRHGNTRIHT`;
                const embed = new MessageEmbed()
                    .setAuthor(
                        msg.author.username,
                        msg.author.displayAvatarURL({ format: "webp" })
                    )
                    .setTitle(message)
                    .setImage(response.data.link)
                    .setFooter(`HE HUGGED MEEEEEEEEEEEEEEE`);
                await msg.embed(embed);
            }
        } catch (e) {
            return msg.say(e.message);
        }
    }
}
