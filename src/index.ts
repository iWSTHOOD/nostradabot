import { CommandoClient } from "discord.js-commando";
import * as path from "path";
import * as dotenv from 'dotenv';

dotenv.config({ path: "../.env" });
const { NOSTRADABOT_PREFIX, NOSTRADABOT_OWNER_ID, NOSTRADABOT_TOKEN, NOSTRADABOT_SERVER_INVITE } = process.env;

const client = new CommandoClient({
    commandPrefix: NOSTRADABOT_PREFIX,
    owner: NOSTRADABOT_OWNER_ID.split(", "),
    commandEditableDuration: 1,
    fetchAllMembers: true,
    disableEveryone: true,
    invite: NOSTRADABOT_SERVER_INVITE,
    retryLimit: 1
});

client.once("ready", () => {
    client.user.setActivity("HOTEL DIABLO", { type: 'LISTENING' });
    client.user.presence.status = "online";
});

client.on("ready", () => {
    console.log("my body is ready");
});

client.on("disconnect", (e: Event) => {
    console.warn("I GOT DISCONNECTED");
    process.exit(0);
});

client.on("error", (error: Error) => {
    console.error("AN ERROR OCCURED");
});

client.registry
	.registerDefaultTypes()
	.registerGroups([
		['userinfo', 'USER INFORMATION'],
        ['fun', 'FUN GAMES'],
        ['random', 'RANDOM SHIT'],
        ['gif', 'WEEB COMMUNICATION'],
        ['fax', 'FACTS LET\'S TALK ABOUT IT'],
	])
	.registerDefaultGroups()
	.registerDefaultCommands({
        commandState: false,
        eval: false,
        ping: false,
    })
	.registerCommandsIn(path.join(__dirname, "commands"));

function shutDown() {
    console.log("I WAS ASKED TO SHUTDOWN");
    client.destroy();
}

process.once("SIGTERM", shutDown);
process.once("SIGINT", shutDown);

client.login(NOSTRADABOT_TOKEN);
